# frozen_string_literal: true

guard :bundler do
  require 'guard/bundler'
  require 'guard/bundler/verify'
  helper = Guard::Bundler::Verify.new

  files = %w[Gemfile]
  files += Dir['*.gemspec'] if files.any? { |f| helper.uses_gemspec?(f) }

  # Assume files are symlinked from somewhere
  files.each { |file| watch(helper.real_path(file)) }
end

guard :rubocop, hide_stdout: true, cli: '--auto-correct --format quiet' do
  watch(/^(.+)\.rb$/)
  watch(%r{(?:.+/)?\.rubocop(?:_todo)?\.yml$}) { |m| File.dirname(m[0]) }
  watch(/^Gemfile.*/)
end

guard :rspec, cmd: 'bundle exec rspec' do
  watch(%r{^spec/(.+)_spec\.rb$}) { |m| "spec/#{m[1]}_spec.rb" }
  watch(%r{^lib/(.+)\.rb$}) { |m| "spec/#{m[1]}_spec.rb" }
  watch(/^Gemfile.*/) { 'spec' }
  watch('spec/support/**/*') { 'spec' }
  watch('spec/spec_helper.rb') { 'spec' }
end
