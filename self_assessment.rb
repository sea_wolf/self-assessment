# frozen_string_literal: true

require 'bigdecimal'
require 'bigdecimal/util'

require 'zeitwerk'
loader = Zeitwerk::Loader.new
loader.push_dir('lib')
loader.enable_reloading # you need to opt-in before setup
loader.setup

Zeitwerk::Loader.eager_load_all
