# frozen_string_literal: true

module SelfAssessment
  module MonkeyPatches
    # YAML engine
    module Psych
      # Changes YAML to deserialise a Float as a BigDecimal
      module FloatToBigDecimal
        def deserialize(original)
          super(original).tap do |deserialized|
            return BigDecimal(original.value) if deserialized.is_a?(Float)
          end
        end
      end

      ::Psych::Visitors::ToRuby.prepend(FloatToBigDecimal)
    end
  end
end
