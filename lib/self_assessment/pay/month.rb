# frozen_string_literal: true

module SelfAssessment
  class Pay
    # Payslip within a Tax Year
    class Month
      include Named

      attr_reader :number_in_tax_year

      def initialize(number_in_tax_year, data)
        @number_in_tax_year = number_in_tax_year
        @_data = data
      end

      def valid?
        return false if _data.keys.include?('total') && total != _data['total']

        payments.total == payments.raw_total &&
          deductions.total == deductions.raw_total
      end

      def total
        0.0.to_d + payments.total - deductions.total
      end

      def income
        payments.entries.inject(BigDecimal(0)) { |sum, entry| sum + entry.value }
      end

      def salary
        payments.salaries.inject(BigDecimal(0)) { |sum, entry| sum + entry.value }
      end

      def bonus
        payments.bonuses.inject(BigDecimal(0)) { |sum, entry| sum + entry.value }
      end

      def outgoing
        deductions.entries.inject(BigDecimal(0)) { |sum, entry| sum + entry.value }
      end

      def paye_tax
        deductions.paye_tax.inject(BigDecimal(0)) { |sum, entry| sum + entry.value }
      end

      def ni_contribution
        deductions.ni_contribution.inject(BigDecimal(0)) { |sum, entry| sum + entry.value }
      end

      def pension
        deductions.pension.inject(BigDecimal(0)) { |sum, entry| sum + entry.value }
      end

      def student_loan
        deductions.student_loan.inject(BigDecimal(0)) { |sum, entry| sum + entry.value }
      end

      private

      attr_reader :_data

      def payments
        Payments.new(_data['payments'])
      end

      def deductions
        Deductions.new(_data['deductions'])
      end
    end
  end
end
