# frozen_string_literal: true

module SelfAssessment
  class Pay
    # A line on a payslip
    class Entry
      attr_reader :name, :value

      def initialize(name, value)
        @name = name.to_s
        @value = value.to_d
      end
    end
  end
end
