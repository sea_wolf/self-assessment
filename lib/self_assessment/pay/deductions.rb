# frozen_string_literal: true

module SelfAssessment
  class Pay
    # Things that are subtracted from the salary
    class Deductions
      include Totalable

      totals_for :paye_tax, :ni_contribution, :pension, :student_loan, collection: :entries

      attr_reader :entries

      def initialize(hash)
        @entries = hash.map { |name, value| Entry.new(name, value.to_d) }
      end

      def paye_tax
        entries.select { |e| e.name == 'paye_tax' }
      end

      def ni_contribution
        entries.select { |e| e.name == 'ni_contribution' }
      end

      def pension
        entries.select { |e| e.name == 'pension' }
      end

      def student_loan
        entries.select { |e| e.name == 'student_loan' }
      end
    end
  end
end
