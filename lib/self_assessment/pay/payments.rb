# frozen_string_literal: true

module SelfAssessment
  class Pay
    # Things are added to the salary
    class Payments
      include Totalable

      totals_for :salary, :bonus, collection: :entries

      attr_reader :entries

      def initialize(hash)
        @entries = hash.map { |name, value| Entry.new(name, value.to_d) }
      end

      def salaries
        entries.select { |e| e.name == 'salary' }
      end

      def bonuses
        entries.select { |e| e.name == 'bonus' }
      end
    end
  end
end
