# frozen_string_literal: true

module SelfAssessment
  class Pay
    # Things have a list of numbers that can be added to a total
    module Totalable
      def self.included(base)
        base.extend(TotalsFor)
        base.include(Total)
      end

      # Provides .totals_for(:attr_1, :attr_2, collection: :data_source_method)
      module TotalsFor
        def totals_for(*attrs, **opts)
          instance_variable_set('@_totals_for_collection', opts[:collection])
          instance_variable_set('@_totals_for_attrs', attrs.map(&:to_s))
        end
      end

      # Provides #total that sums the `totals_for` list
      module Total
        def total
          @total ||= begin
            collection_name = self.class.instance_variable_get('@_totals_for_collection')
            collection = send(collection_name.to_sym)

            entries = self.class.instance_variable_get('@_totals_for_attrs')

            collection.inject(0.0) do |sum, entry|
              next sum unless entries.include?(entry.name)

              sum + entry.value
            end
          end
        end

        def raw_total
          collection = entries.select { |e| e.name == 'total' }
          return total unless collection.any?

          collection.sum(&:value)
        end
      end
    end
  end
end
