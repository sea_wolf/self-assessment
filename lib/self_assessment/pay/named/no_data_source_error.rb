# frozen_string_literal: true

module SelfAssessment
  class Pay
    module Named
      # The including class doesn't have anything to supply a `name`
      class NoDataSourceError < StandardError
        MESSAGE = 'No "data" or "_data" source for `name` method'
        def message
          MESSAGE
        end
      end
    end
  end
end
