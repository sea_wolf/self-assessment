# frozen_string_literal: true

module SelfAssessment
  class Pay
    # Things that have a name
    module Named
      def name
        data_source = if instance_variables.include?(:@data)
                        instance_variable_get(:@data)
                      elsif instance_variables.include?(:@_data)
                        instance_variable_get(:@_data)
                      else
                        raise NoDataSourceError
                      end

        data_source['name']
      end
    end
  end
end
