# frozen_string_literal: true

module SelfAssessment
  class Pay
    # Collection of monthly payslips to make up a Tax Year
    class Year
      include Named

      def initialize(data)
        @_data = data
      end

      def total
        @total ||= months.inject(BigDecimal(0)) { |sum, month| sum + month.total }
      end

      def income
        @income ||= months.inject(BigDecimal(0)) { |sum, month| sum + month.income }
      end

      def salary
        @salary ||= months.inject(BigDecimal(0)) { |sum, month| sum + month.salary }
      end

      def bonus
        @bonus ||= months.inject(BigDecimal(0)) { |sum, month| sum + month.bonus }
      end

      def outgoing
        @outgoing ||= months.inject(BigDecimal(0)) { |sum, month| sum + month.outgoing }
      end

      def paye_tax
        @paye_tax ||= months.inject(BigDecimal(0)) { |sum, month| sum + month.paye_tax }
      end

      def ni_contribution
        @ni_contribution ||= months.inject(BigDecimal(0)) { |sum, month| sum + month.ni_contribution }
      end

      def pension
        @pension ||= months.inject(BigDecimal(0)) { |sum, month| sum + month.pension }
      end

      def student_loan
        @student_loan ||= months.inject(BigDecimal(0)) { |sum, month| sum + month.student_loan }
      end

      private

      attr_reader :_data

      def months
        @months ||= _data['months'].map { |num, data| Month.new(num, data) }
      end
    end
  end
end
