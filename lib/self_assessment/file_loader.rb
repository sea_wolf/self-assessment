# frozen_string_literal: true

require 'yaml'

module SelfAssessment
  # Loads file for storing sensative data
  module FileLoader
    def self.included(base)
      base.attr_reader :data
    end

    def initialize(*args)
      @data = data_file

      super(*args)
    end

    def data_file_basename
      file = self.class.name.split('::').last.downcase
      "#{file}.yml"
    end

    def find_data_file
      File.join(File.dirname(__FILE__), data_file_basename)
    end

    def read_data_file
      path = find_data_file
      File.read(path)
    rescue StandardError => e
      warn e.message
      '--- {}'
    end

    def load_data_file
      file_contents = read_data_file
      YAML.safe_load(file_contents)
    rescue TypeError => e
      warn e.message
      YAML.safe_load('--- {}')
    end

    def data_file
      load_data_file.tap do |data|
        unless data.is_a?(Hash)
          warn 'Invalid file!'
          return {}
        end
      end
    end
  end
end
