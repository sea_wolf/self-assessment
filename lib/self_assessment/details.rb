# frozen_string_literal: true

module SelfAssessment
  # Personal details
  class Details
    include FileLoader

    def ni_number
      data['ni_number']
    end
  end
end
