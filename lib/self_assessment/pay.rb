# frozen_string_literal: true

module SelfAssessment
  # Payslip data
  class Pay
    include FileLoader

    def years
      @years ||= data.fetch('years', {}).map { |_id, year| Year.new(year) }
    end
  end
end
