# frozen_string_literal: true

module SelfAssessment
  module RSpec
    module Warning
      def warn(*_args); end
    end
  end
end

RSpec.configure do |config|
  config.before do
    ::Warning.extend(SelfAssessment::RSpec::Warning)
  end
end
