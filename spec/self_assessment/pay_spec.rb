# frozen_string_literal: true

describe SelfAssessment::Pay do
  describe '#years' do
    subject(:years) { instance.years }

    let(:instance) { described_class.new }

    before do
      allow(instance).to receive(:data).and_return(example_data)
    end

    context 'with a number of yers in the data file' do
      let(:example_data) do
        {
          'years' => {
            '1': {
              'name' => '2001'
            },
            '2': {
              'name' => '2002'
            },
            '3': {
              'name' => '2003'
            }
          }
        }
      end

      it 'has each year from the data file' do
        expect(years).to contain_exactly(
          have_attributes(class: described_class::Year, name: '2001'),
          have_attributes(class: described_class::Year, name: '2002'),
          have_attributes(class: described_class::Year, name: '2003')
        )
      end
    end

    context 'with empty data' do
      let(:example_data) { {} }

      it 'is an empty collection' do
        expect(years).to be_empty
      end
    end
  end
end
