# frozen_string_literal: true

describe SelfAssessment::Pay::Entry do
  let(:instance) { described_class.new(:something, 123) }

  describe '#name' do
    subject { instance.name }

    it { is_expected.to be_a(String) }
  end

  describe '#value' do
    subject { instance.value }

    it { is_expected.to be_a(BigDecimal) }
  end
end
