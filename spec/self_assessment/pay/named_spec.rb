# frozen_string_literal: true

module SelfAssessment
  module RSpec
    class NamedExampleWithPublicData
      include ::SelfAssessment::Pay::Named

      def initialize
        @data = { 'name' => 'Hello from @data' }
      end
    end

    class NamedExampleWithPrivateData
      include ::SelfAssessment::Pay::Named

      def initialize
        @_data = { 'name' => 'Hello from @_data' }
      end
    end

    class NamedExampleWithPublicAndPrivateData
      include ::SelfAssessment::Pay::Named

      def initialize
        @data = { 'name' => 'Hello from @data' }
        @_data = { 'name' => 'Hello from @_data' }
      end
    end

    class NamedExampleWithNoData
      include ::SelfAssessment::Pay::Named
    end
  end
end

describe SelfAssessment::Pay::Named do
  describe '#name' do
    subject(:name) { instance.name }

    context 'with public @data' do
      let(:instance) { SelfAssessment::RSpec::NamedExampleWithPublicData.new }

      it 'extracts the name from it' do
        expect(name).to eq('Hello from @data')
      end
    end

    context 'with private @_data' do
      let(:instance) { SelfAssessment::RSpec::NamedExampleWithPrivateData.new }

      it 'extracts the name from it' do
        expect(name).to eq('Hello from @_data')
      end
    end

    context 'with both public @data and private @_data' do
      let(:instance) { SelfAssessment::RSpec::NamedExampleWithPublicAndPrivateData.new }

      it 'extracts the name from the public @data' do
        expect(name).to eq('Hello from @data')
      end
    end

    context 'with neither data source' do
      let(:instance) { SelfAssessment::RSpec::NamedExampleWithNoData.new }

      it 'raises an informative error' do
        expect { name }.to raise_error(described_class::NoDataSourceError)
      end
    end
  end
end
