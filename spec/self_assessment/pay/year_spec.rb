# frozen_string_literal: true

describe SelfAssessment::Pay::Year do
  let(:instance) { described_class.new(data) }
  let(:data) do
    {
      'months' => {
        '1' => {
          'name' => '28/03/2019 - 27/04/2019',
          'payments' => {
            'salary' => 234.50
          },
          'deductions' => {
            'paye_tax' => 21.33,
            'ni_contribution' => 12.09,
            'pension' => 56.20,
            'student_loan' => 88.00
          }
        },
        '2' => {
          'name' => '28/04/2019 - 27/05/2019',
          'payments' => {
            'salary' => 234.50,
            'bonus' => 12.34
          },
          'deductions' => {
            'paye_tax' => 24.66,
            'ni_contribution' => 14.18,
            'pension' => 58.24,
            'student_loan' => 89.22
          }
        }
      }
    }
  end

  describe '#total' do
    subject(:total) { instance.total }

    it { is_expected.to be_a(BigDecimal) }

    it "is the sum of each months' payments less the sum of each months' deductions" do
      expect(total).to eq(BigDecimal('117.42'))
    end
  end

  describe '#income' do
    subject(:income) { instance.income }

    it { is_expected.to be_a(BigDecimal) }

    it "is the sum of each months' payments" do
      expect(income).to eq(BigDecimal('481.34'))
    end
  end

  describe '#salary' do
    subject(:salary) { instance.salary }

    it { is_expected.to be_a(BigDecimal) }

    it "is the sum of each months' salary payment" do
      expect(salary).to eq(BigDecimal('469'))
    end
  end

  describe '#bonus' do
    subject(:bonus) { instance.bonus }

    it { is_expected.to be_a(BigDecimal) }

    it "is the sum of each months' bonus payment" do
      expect(bonus).to eq(BigDecimal('12.34'))
    end
  end

  describe '#outgoing' do
    subject(:outgoing) { instance.outgoing }

    it { is_expected.to be_a(BigDecimal) }

    it "is the sum of each months' deductions" do
      expect(outgoing).to eq(BigDecimal('363.92'))
    end
  end

  describe '#paye_tax' do
    subject(:paye_tax) { instance.paye_tax }

    it { is_expected.to be_a(BigDecimal) }

    it "is the sum of each months' PAYE tax deduction" do
      expect(paye_tax).to eq(BigDecimal('45.99'))
    end
  end

  describe '#ni_contribution' do
    subject(:ni_contribution) { instance.ni_contribution }

    it { is_expected.to be_a(BigDecimal) }

    it "is the sum of each months' National Insurance deduction" do
      expect(ni_contribution).to eq(BigDecimal('26.27'))
    end
  end

  describe '#pension' do
    subject(:pension) { instance.pension }

    it { is_expected.to be_a(BigDecimal) }

    it "is the sum of each months' Pension deduction" do
      expect(pension).to eq(BigDecimal('114.44'))
    end
  end

  describe '#student_loan' do
    subject(:student_loan) { instance.student_loan }

    it { is_expected.to be_a(BigDecimal) }

    it "is the sum of each months' Student Loan deduction" do
      expect(student_loan).to eq(BigDecimal('177.22'))
    end
  end
end
