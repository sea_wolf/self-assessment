# frozen_string_literal: true

describe SelfAssessment::Pay::Month do
  describe '#valid?' do
    subject { described_class.new(0, data) }

    context 'with no totals' do
      let(:data) do
        {
          'name' => '28/03/2019 - 27/04/2019',
          'payments' => {
            'salary' => 234.50
          },
          'deductions' => {
            'paye_tax' => 21.33,
            'ni_contribution' => 12.09,
            'pension' => 56.20,
            'student_loan' => 88.00
          }
        }
      end

      it { is_expected.to be_valid }
    end

    context 'with a matching monthly total' do
      let(:data) do
        {
          'name' => '28/03/2019 - 27/04/2019',
          'payments' => {
            'salary' => 234.50
          },
          'deductions' => {
            'paye_tax' => 21.33,
            'ni_contribution' => 12.09,
            'pension' => 56.20,
            'student_loan' => 88.00
          },
          'total' => 56.88
        }
      end

      it { is_expected.to be_valid }
    end

    context 'with a mismatching monthly total' do
      let(:data) do
        {
          'name' => '28/03/2019 - 27/04/2019',
          'payments' => {
            'salary' => 234.50
          },
          'deductions' => {
            'paye_tax' => 21.33,
            'ni_contribution' => 12.09,
            'pension' => 56.20,
            'student_loan' => 88.00
          },
          'total' => 56.89
        }
      end

      it { is_expected.not_to be_valid }
    end

    context 'with a matching monthly total and a matching payments total' do
      let(:data) do
        {
          'name' => '28/03/2019 - 27/04/2019',
          'payments' => {
            'salary' => 234.50,
            'total' => 234.50
          },
          'deductions' => {
            'paye_tax' => 21.33,
            'ni_contribution' => 12.09,
            'pension' => 56.20,
            'student_loan' => 88.00
          },
          'total' => 56.88
        }
      end

      it { is_expected.to be_valid }
    end

    context 'with a matching monthly total and a mismatching payments total' do
      let(:data) do
        {
          'name' => '28/03/2019 - 27/04/2019',
          'payments' => {
            'salary' => 234.50,
            'total' => 234.51
          },
          'deductions' => {
            'paye_tax' => 21.33,
            'ni_contribution' => 12.09,
            'pension' => 56.20,
            'student_loan' => 88.00
          },
          'total' => 56.88
        }
      end

      it { is_expected.not_to be_valid }
    end

    context 'with a mismatching monthly total and a matching payments total' do
      let(:data) do
        {
          'name' => '28/03/2019 - 27/04/2019',
          'payments' => {
            'salary' => 234.50,
            'total' => 234.50
          },
          'deductions' => {
            'paye_tax' => 21.33,
            'ni_contribution' => 12.09,
            'pension' => 56.20,
            'student_loan' => 88.00
          },
          'total' => 56.89
        }
      end

      it { is_expected.not_to be_valid }
    end

    context 'with a mismatching monthly total and a mismatching payments total' do
      let(:data) do
        {
          'name' => '28/03/2019 - 27/04/2019',
          'payments' => {
            'salary' => 234.50,
            'total' => 234.51
          },
          'deductions' => {
            'paye_tax' => 21.33,
            'ni_contribution' => 12.09,
            'pension' => 56.20,
            'student_loan' => 88.00
          },
          'total' => 56.89
        }
      end

      it { is_expected.not_to be_valid }
    end

    context 'with a matching monthly total and a matching deductions total' do
      let(:data) do
        {
          'name' => '28/03/2019 - 27/04/2019',
          'payments' => {
            'salary' => 234.50
          },
          'deductions' => {
            'paye_tax' => 21.33,
            'ni_contribution' => 12.09,
            'pension' => 56.20,
            'student_loan' => 88.00,
            'total' => 177.62
          },
          'total' => 56.88
        }
      end

      it { is_expected.to be_valid }
    end

    context 'with a matching monthly total and a mismatching deductions total' do
      let(:data) do
        {
          'name' => '28/03/2019 - 27/04/2019',
          'payments' => {
            'salary' => 234.50
          },
          'deductions' => {
            'paye_tax' => 21.33,
            'ni_contribution' => 12.09,
            'pension' => 56.20,
            'student_loan' => 88.00,
            'total' => 177.63
          },
          'total' => 56.88
        }
      end

      it { is_expected.not_to be_valid }
    end

    context 'with a mismatching monthly total and a matching deductions total' do
      let(:data) do
        {
          'name' => '28/03/2019 - 27/04/2019',
          'payments' => {
            'salary' => 234.50
          },
          'deductions' => {
            'paye_tax' => 21.33,
            'ni_contribution' => 12.09,
            'pension' => 56.20,
            'student_loan' => 88.00,
            'total' => 177.62
          },
          'total' => 56.89
        }
      end

      it { is_expected.not_to be_valid }
    end

    context 'with a mismatching monthly total and a mismatching deductions total' do
      let(:data) do
        {
          'name' => '28/03/2019 - 27/04/2019',
          'payments' => {
            'salary' => 234.50
          },
          'deductions' => {
            'paye_tax' => 21.33,
            'ni_contribution' => 12.09,
            'pension' => 56.20,
            'student_loan' => 88.00,
            'total' => 177.63
          },
          'total' => 56.89
        }
      end

      it { is_expected.not_to be_valid }
    end

    context 'with a matching monthly total and a matching payments total and a matching deductions total' do
      let(:data) do
        {
          'name' => '28/03/2019 - 27/04/2019',
          'payments' => {
            'salary' => 234.50,
            'total' => 234.50
          },
          'deductions' => {
            'paye_tax' => 21.33,
            'ni_contribution' => 12.09,
            'pension' => 56.20,
            'student_loan' => 88.00,
            'total' => 177.62
          },
          'total' => 56.88
        }
      end

      it { is_expected.to be_valid }
    end

    context 'with a matching monthly total and a mismatching payments total and a matching deductions total' do
      let(:data) do
        {
          'name' => '28/03/2019 - 27/04/2019',
          'payments' => {
            'salary' => 234.50,
            'total' => 234.51
          },
          'deductions' => {
            'paye_tax' => 21.33,
            'ni_contribution' => 12.09,
            'pension' => 56.20,
            'student_loan' => 88.00,
            'total' => 177.62
          },
          'total' => 56.88
        }
      end

      it { is_expected.not_to be_valid }
    end

    context 'with a matching monthly total and a matching payments total and a mismatching deductions total' do
      let(:data) do
        {
          'name' => '28/03/2019 - 27/04/2019',
          'payments' => {
            'salary' => 234.50,
            'total' => 234.50
          },
          'deductions' => {
            'paye_tax' => 21.33,
            'ni_contribution' => 12.09,
            'pension' => 56.20,
            'student_loan' => 88.00,
            'total' => 177.63
          },
          'total' => 56.88
        }
      end

      it { is_expected.not_to be_valid }
    end

    context 'with a matching monthly total and a mismatching payments total and a mismatching deductions total' do
      let(:data) do
        {
          'name' => '28/03/2019 - 27/04/2019',
          'payments' => {
            'salary' => 234.50,
            'total' => 234.51
          },
          'deductions' => {
            'paye_tax' => 21.33,
            'ni_contribution' => 12.09,
            'pension' => 56.20,
            'student_loan' => 88.00,
            'total' => 177.63
          },
          'total' => 56.88
        }
      end

      it { is_expected.not_to be_valid }
    end

    context 'with a mismatching monthly total and a matching payments total and a matching deductions total' do
      let(:data) do
        {
          'name' => '28/03/2019 - 27/04/2019',
          'payments' => {
            'salary' => 234.50,
            'total' => 234.50
          },
          'deductions' => {
            'paye_tax' => 21.33,
            'ni_contribution' => 12.09,
            'pension' => 56.20,
            'student_loan' => 88.00,
            'total' => 177.62
          },
          'total' => 56.89
        }
      end

      it { is_expected.not_to be_valid }
    end

    context 'with a mismatching monthly total and a mismatching payments total and a matching deductions total' do
      let(:data) do
        {
          'name' => '28/03/2019 - 27/04/2019',
          'payments' => {
            'salary' => 234.50,
            'total' => 234.51
          },
          'deductions' => {
            'paye_tax' => 21.33,
            'ni_contribution' => 12.09,
            'pension' => 56.20,
            'student_loan' => 88.00,
            'total' => 177.62
          },
          'total' => 56.89
        }
      end

      it { is_expected.not_to be_valid }
    end

    context 'with a mismatching monthly total and a matching payments total and a mismatching deductions total' do
      let(:data) do
        {
          'name' => '28/03/2019 - 27/04/2019',
          'payments' => {
            'salary' => 234.50,
            'total' => 234.50
          },
          'deductions' => {
            'paye_tax' => 21.33,
            'ni_contribution' => 12.09,
            'pension' => 56.20,
            'student_loan' => 88.00,
            'total' => 177.63
          },
          'total' => 56.89
        }
      end

      it { is_expected.not_to be_valid }
    end

    context 'with a mismatching monthly total and a mismatching payments total and a mismatching deductions total' do
      let(:data) do
        {
          'name' => '28/03/2019 - 27/04/2019',
          'payments' => {
            'salary' => 234.50,
            'total' => 234.51
          },
          'deductions' => {
            'paye_tax' => 21.33,
            'ni_contribution' => 12.09,
            'pension' => 56.20,
            'student_loan' => 88.00,
            'total' => 177.63
          },
          'total' => 56.89
        }
      end

      it { is_expected.not_to be_valid }
    end
  end
end
