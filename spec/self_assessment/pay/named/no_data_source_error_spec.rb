# frozen_string_literal: true

describe SelfAssessment::Pay::Named::NoDataSourceError do
  it { is_expected.to be_a(StandardError) }

  describe '#message' do
    subject { described_class.new.message }

    it { is_expected.to eq('No "data" or "_data" source for `name` method') }
  end
end
