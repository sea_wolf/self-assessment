# frozen_string_literal: true

describe SelfAssessment::Details do
  let(:example_data) { File.read('./spec/support/details_example.yml') }

  before do
    allow(File).to receive(:read).with(%r{.+/details.yml}).and_return(example_data)
  end

  describe '#ni_number' do
    subject(:ni_number) { described_class.new.ni_number }

    it 'extracts the data from the "ni_number" value' do
      expect(ni_number).to eq('ABC123')
    end
  end
end
