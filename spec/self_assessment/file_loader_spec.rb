# frozen_string_literal: true

module SelfAssessment
  module RSpec
    class LoaderExample
      include ::SelfAssessment::FileLoader
    end
  end
end

describe SelfAssessment::FileLoader do
  describe '#data' do
    subject(:data) { instance.data }

    let(:instance) { SelfAssessment::RSpec::LoaderExample.new }

    context 'with a good file' do
      let(:example_data) { File.read('./spec/support/loader_example.yml') }

      before do
        allow(File).to receive(:read).with(%r{.+/loaderexample.yml}).and_return(example_data)
      end

      it 'loads the YAML file of the name given' do
        expect(data).to eq({ 'this' => 'works!' })
      end
    end

    context 'with a useless file' do
      let(:example_data) { File.read('./spec/support/loader_bad_example.yml') }

      before do
        allow(File).to receive(:read).with(%r{.+/loaderexample.yml}).and_return(example_data)
      end

      it 'loads the YAML file of the name given' do
        expect(data).to eq({})
      end
    end

    context 'with an unparseable file' do
      let(:example_data) { nil }

      before do
        allow(File).to receive(:read).with(%r{.+/loaderexample.yml}).and_return(example_data)
      end

      it 'loads the YAML file of the name given' do
        expect(data).to eq({})
      end
    end

    context 'with no file' do
      it 'loads the YAML file of the name given' do
        expect(data).to eq({})
      end
    end
  end
end
