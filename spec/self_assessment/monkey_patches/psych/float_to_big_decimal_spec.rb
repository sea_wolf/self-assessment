# frozen_string_literal: true

describe SelfAssessment::MonkeyPatches::Psych::FloatToBigDecimal do
  describe 'overridden method' do
    subject { ::Psych::Visitors::ToRuby.ancestors }

    it { is_expected.to be_include(described_class) }
  end

  describe '#deserialize' do
    subject { YAML.safe_load(yaml, allowed_classes).first }

    let(:allowed_classes) { [] }
    let(:yaml) { [value].to_yaml }

    context 'when deserialising a Float' do
      let(:value) { 12.34 }

      it { is_expected.to be_a(BigDecimal) }
    end

    context 'when deserialising a BigDecimal' do
      let(:allowed_classes) { [BigDecimal] }
      let(:value) { BigDecimal('12.34') }

      it { is_expected.to be_a(BigDecimal) }
    end

    context 'when deserialising an Integer' do
      let(:value) { 1234 }

      it { is_expected.to be_a(Integer) }
    end

    context 'when deserialising a String' do
      let(:value) { '12.34' }

      it { is_expected.to be_a(String) }
    end

    context 'when deserialising a NilClass' do
      let(:value) { nil }

      it { is_expected.to be_nil }
    end
  end
end
